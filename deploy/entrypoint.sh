#!/bin/sh
set -e

echo "* * * * * cd /var/www/app && /usr/local/bin/php artisan schedule:run >> /dev/null" >> /etc/crontabs/root
#php artisan migrate --force
/usr/bin/supervisord -c /etc/supervisord.conf