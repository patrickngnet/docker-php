FROM surnet/alpine-wkhtmltopdf:3.9-0.12.5-small as wkhtmltopdf
#FROM php:7.4-fpm-alpine3.13 as app 
FROM php:8.2.1-fpm-alpine3.17 as app 

WORKDIR /var/www/app

RUN apk update \
    && apk add git \
    supervisor \
    bash \
    nginx \
    curl \
    libbz2 \
    libzip \
    zlib \
    libpng \
    libpng-dev \
    libjpeg-turbo-dev \
    libwebp-dev \
    libzip-dev \
    zlib-dev \
    zip \
    unzip \
#docker install extension    
    && docker-php-ext-install bcmath \
    mysqli \
    opcache \
    pdo \
    pdo_mysql \
    gd \
    zip \
    && rm -rf /var/cache/apk/*
    
RUN apk add \
    libstdc++ \
    libx11 \
    libxrender \
    libxext \
    libssl1.1 \
    ca-certificates \
    fontconfig \
    freetype \
    ttf-dejavu \
    ttf-droid \
    ttf-freefont \
    ttf-liberation \
    #ttf-ubuntu-font-family \
    && apk add --virtual .build-deps \
    msttcorefonts-installer \
    # Install microsoft fonts
    && update-ms-fonts \
    && fc-cache -f \
    # Clean up when done
    && rm -rf /tmp/* \
    && apk del .build-deps

#configure wkhtmltopdf 
COPY --from=wkhtmltopdf /bin/wkhtmltopdf /usr/local/bin/wkhtmltopdf

#configure composer 
COPY --from=composer /usr/bin/composer /usr/bin/composer

COPY ./deploy/supervisord.conf /etc/supervisord.conf

#setup nginx configuration
COPY ./deploy/nginx-site.conf /etc/nginx/http.d/default.conf

#setup php-fpm configuration
COPY ./deploy/php-fpm.d/ /usr/local/etc/php-fpm.d

COPY ./deploy/entrypoint.sh /etc/entrypoint.sh

RUN mv "$PHP_INI_DIR/php.ini-production" "$PHP_INI_DIR/php.ini" \
    #setup nginx
    && mkdir -p /run/nginx \
    && chmod 755 /etc/nginx/http.d/default.conf \
    #setup laravel
    #&& composer install --ignore-platform-reqs --prefer-dist --no-scripts -q -o  \
    #&& cp .env.ecs.dev .env \
    #&& chmod -R 777 storage \
    #&& php artisan optimize \
    #&& chown -R www-data:www-data /var/www \
    &&  chmod +x /etc/entrypoint.sh

EXPOSE 80

#ENTRYPOINT ["/etc/entrypoint.sh"]